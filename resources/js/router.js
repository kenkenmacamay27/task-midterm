
import Login from '../js/components/pages/Login'
import Register from '../js/components/pages/Register'
import Dashboard from '../js/components/pages/Dashboard'
import NotFound from '../js/components/pages/NotFound'


const routes = [
    {
        path: '*',
        name: 'NotFound',
        component: NotFound
    },
    {
        path: '/login',
        name: 'Login',
        component: Login
    },
    {
        path: '/register',
        name: 'Register',
        component: Register
    },
    {
        path: '/',
        name: 'Dashboard',
        component: Dashboard
    },
]

export default routes
