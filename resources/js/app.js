require("./bootstrap");

import Vue from "vue";
import VueRouter from "vue-router";
import Main from "../js/components/pages/Main";
import routes from "./router";
import store from './store'
import toastr from './toastr'

Vue.use(toastr)
Vue.use(VueRouter);
Vue.component('pagination', require('laravel-vue-pagination'));

import 'toastr/build/toastr.css'

const router = new VueRouter({
    history: true,
    mode: "history",
    routes
});

new Vue({
    el: "#app",
    router,
    store,
    render: app => app(Main)
});
