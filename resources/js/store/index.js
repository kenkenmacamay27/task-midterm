import Vuex from "vuex";
import Vue from "vue";
import user from "./modules/user";
import token from "./modules/token";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        user,
        token
    }
});
