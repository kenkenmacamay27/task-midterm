const state = {
    token: localStorage.getItem("token") || ""
};

const getters = {
    getToken: state => state.token
};

const actions = {
    
};

const mutations = {
    //Token
    setToken: (state, token) => {
        localStorage.setItem("token", token);
        state.token = token;
    },
    clearToken: state => {
        localStorage.removeItem("token");
        state.token = "";
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
