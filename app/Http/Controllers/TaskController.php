<?php

namespace App\Http\Controllers;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class TaskController extends Controller
{
        /**
     * Create a new TaskController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('api');
    }

    public function get(Request $request) {
        return Task::where('user_id', $request->user_id)->paginate(5);
    }

    public function total(Request $request) {
        return response()->json(['total_task' => Task::all()->where('user_id', $request->user_id)->count()]);
    }

    public function store(Request $request) {
        
        $validate = Validator::make($request->all(), [
            'user_id'  => 'required',
            'task.name'  => 'required',
            'task.description'  => 'required',
            'task.completed'  => 'required',
            
        ]);
        if ($validate->fails())
        {
            $message = $validate->errors()->getMessages();

            return response()->json([
                'success' => false,
                'messages' => $message 
            ], 200);
        }
        Task::create([
            'user_id' => $request->user_id,
            'name' => $request->task['name'],
            'description' => $request->task['description'],
            'completed' => $request->task['completed']
        ]);

        return response()->json(['success' => true, 'message' => 'Task Added!'], 200);
    }

    public function delete(Request $request) {
        Task::where('id', $request->id)->where('user_id', $request->user_id)->delete();
        return response()->json([ 'message' => 'Task Deleted!'], 200);
    }

    public function completeTask(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'completed'  => 'required',
            
        ]);
        if ($validate->fails())
        {
            $message = $validate->errors()->getMessages();

            return response()->json([
                'success' => false,
                'messages' => $message 
            ], 200);
        }
        $category = [
            'completed' => $request->completed,
        ];
        Task::where('id', $request->id)->where('user_id', $request->user_id)->update($category);

        return response()->json(['success' => true, 'message' => 'Task Updated!', 'category' => $category], 200);
    }

}
